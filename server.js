/* 

  # Example for Bealder's connector

  # Description 
	This server connect to Bealder's connector to get information when beacon is detected

  # Module dependencies:
  - Express
  - Http (to run Express)
  - Body parser (to parse JSON requests)
  - Underscore (because it's cool)
  - Socket.IO

  # Author 
   - Julien <julien@bealder.com>

*/
var express = require("express")
  , app = express()
  , http = require("http").createServer(app)
  , bodyParser = require("body-parser")
  , io = require("socket.io").listen(http)
  , _ = require("underscore");


/* Server config */

//Server's IP address
var url = process.env.BASE_URI || "localhost";
app.set("ipaddr", url);

//Server's port number 

var port = process.env.PORT || 5000;
app.set("port", port);

//Specify the views folder
app.set("views", __dirname + "/views");

//View engine is Jade
app.set("view engine", "jade");

//Specify where the static content is
app.use(express.static("public", __dirname + "/public"));

//Tells server to support JSON requests
app.use(bodyParser.json());
//Tells server to support urlencoded requests 
app.use(bodyParser.urlencoded({ extended: true }));

/* Server routing */

//Handle route "GET /", as in "http://localhost:5000/"
app.get("/", function(request, response) {

  //Render the view called "index"
  response.render("index");

});

//POST method to send information when Beacon is detected
app.post("/beacon", function(request, response) {

	// get Parameters
	var user = request.body.user;
	var user_id = request.body.user.id_connector;
	var beacon_id = request.body.beacon;
	var campaign_id = request.body.campaign;
	var proximity = request.body.proximity;

	//If the message is empty or wasn't sent it's a bad request
	if(_.isUndefined(user)) {
		return response.json(400, {error: "no user"});
	}

	if(proximity == 'immediate' || proximity == 'near'){
		console.log( new Date().toJSON().slice(0,16) + " - Send User "+user.name);
		//Let our chatroom know there was a new message
		io.sockets.emit("beaconDetected", {user: user});		
	}

	//Looks good, let the client know
	response.json(204, {status: "ok"});

});

//Start the http server at port and IP defined before
http.listen(app.get("port"),function() {
  console.log("Server up and running. Go to http://" + app.get("ipaddr") + ":" + app.get("port"));
});