function init() {

  var serverBaseUrl = document.domain;
  var socket = io.connect(serverBaseUrl);

  socket.on('beaconDetected', function (data) {   
    updateUser(data.user);
  });

  function updateUser(user){
    console.log(user);
    var datas = JSON.parse(user.datas);
  	$('#title').html("Welcome "+datas.name+" !");
  	$('#email').html(datas.email);
    $('#like').html("Tu aimes : "+datas.like);
  }
}

$(document).on('ready', init);