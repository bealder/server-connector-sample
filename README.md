# Server Connector - sample #

This is a sample server to connect to Bealder's plateform.

## Server installation ###

### __Require__ ###

> Nodejs install

### Install dependencies ###

   > npm install

### Configure server.js ###

"ipaddr" for example

### run server ###

   > node server.js

## Tools

Install forever to manage run node server

> npm install forever -g

how used forever

> forever start server.js

show server launched

> forever list

show log server

> tail -f /**path_forever*log** 

## inside server

 - One route to get page static (in dir public)

 - One route to connector 
Bealder's api call this route to sent information
In this route, the server emit user information in websocket channel called "beaconDetected"

 - On route for socket.io (create channel between "beacon" route and static page

## inside static page
 - connection to socket.io
 - listen channel "beaconDetected"
 - when new datas will receive, it will update dom HTML with new datas.


 ## Deploy to heroku

 ```
 heroku apps:create my-server-name
 git push heroku master 
 
 ```